﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GI.SMS3K.Interface;
using KLx.App;

namespace Sms3kConversion
{
    public class WorkerCurrentUser : ICurrentUser
    {


        public KLx.App.Interface.IUserCredential GetCurrentUser()
        {
            return new UserCredential
            {
                UserName = "Worker",
                DisplayText = "Worker Role",
                Roles = new string[] { "Worker" },
            };
        }

        public string GetCurrentUserName()
        {
            return "Worker";
        }


        public string RemoteIPAddress
        {
            get { return "Server"; }
        }
    }
}
