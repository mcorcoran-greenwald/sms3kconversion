﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GI.Admin;
using GI.Admin.Interface;
using GI.SMS3K.Domain;
using GI.SMS3K.Interface;
using GI.SMS3K.Repository;
using KLx.App.Azure.Interface;
using Ninject;
using IAdministration = GI.SMS3K.Interface.IAdministration;

namespace Sms3kConversion
{
    public class ConvertEntities
    {
        public static async void Run()
        {
            var convert = new ConvertEntities();
            await convert.CustomerAccountConversion();
            await convert.UserConversion();

        }

        private IKernel _kernel;
        private AdminClient _newAdmin;

        public ConvertEntities()
        {

            var accountName = "sms3ktest";
            var accountKey = "2NWCg+tBZR/I8OR3v1yxOG/8BqXnqb06eTBLmzBwNPQu9W9lU8mENPoAXRgY056MzMAJpjsdEUqHfbfrhJWIBA==";

           
            _kernel = new StandardKernel(new SMS3KModule());

            _kernel.Bind<ICurrentUser>().To<WorkerCurrentUser>().InSingletonScope();

            _kernel.Bind<ICloudStorageProvider>().To<CloudStorageProvider>()
                .InSingletonScope()
                .WithConstructorArgument("accountName", accountName)
                .WithConstructorArgument("accountKey", accountKey);
        }

        public async Task CustomerAccountConversion()
        {
            var gmsAdmin = _kernel.Get<IAdministration>();
            var admin = new AdminClient("http://localhost:51833/");
            //var machines = _kernel.Get<IMachineManager>();
            //var profiles = _kernel.Get<ILegacyProfileManager>();

            var smsCustomers = gmsAdmin.GetCustomers();

            foreach (var smsCustomer in smsCustomers)
            {
                var smsAccounts = gmsAdmin.GetCustomerAccounts(smsCustomer.CustomerID);

                var gmsCounter = await admin.AddCustomerAsync(smsCustomer.Name);

                foreach (var smsAccount in smsAccounts)
                {
                    //Where is hasSMSKey
                    var gmsAccount = await admin.AddAccountAsync(new GI.Admin.Interface.Account
                        {
                            Name = smsAccount.Name,
                            AccountID = smsAccount.AccountID,
                            CustomerID = gmsCounter.CustomerID,
                            CustomerCode = smsAccount.CustomerCode,
                        });

                }
            }
        }

        public async Task UserConversion()
        {
            var gmsAdmin = _kernel.Get<IAdministration>();
            var admin = new AdminClient("http://localhost:51833/");

            var smsUsers = gmsAdmin.GetSystemUsers();

            foreach (var smsUser in smsUsers)
            {
                var gmsUser = new GI.Admin.Interface.User
                {
                    CustomerID = smsUser.CustomerID,
                    Active = smsUser.Active,
                    EmailAddress = smsUser.EmailAddress,
                    FullName = smsUser.FullName,
                    IsSystemUser = smsUser.IsSystemUser,
                    PasswordChangeRequired = smsUser.PasswordChangeRequired,
                    Phone = smsUser.Phone,
                    TimeZone = smsUser.TimeZone,
                    UserName = smsUser.UserName,
                    Roles = smsUser.Roles
                };

                await admin.AddUserAsync(gmsUser);

            }
        }
    }
}